
export default class CPShopMod extends Plugin{

  main(){
      ig.input.bind(ig.KEY.P, 'DEBUG_ADD_CP');// for testing purposes
      ig.game.addons.preUpdate.push(this);
  }

  prestart(){

    sc.Arena.inject({
      onVarAccess(path, keys) {
        if (keys[0] == "arena" && keys[1]) {
          switch (keys[1]) {
            case "coins":
              return this.coins;
          }
        }
        return this.parent(path, keys);
      }
    });

    sc.PlayerModel.inject({
      skillPointsBought: [0, 0, 0, 0, 0],
      getSkillPointsBoughtTotal() {
        return this.skillPointsBought.reduce(function(e, t) {
          return t + e
        }, 0);
      },
      getSkillPointPrice(element) {
        return 500 + (this.skillPointsBought[element] * 250) + (this.getSkillPointsBoughtTotal() * 250);
      },
      getSaveData() {
          const result = this.parent();
          result.skillPointsBought = ig.copy(this.skillPointsBought);
          return result;
      },
      preLoad(a) {
          this.skillPointsBought = ig.copy(a.skillPointsBought) || [0, 0, 0, 0, 0];
          this.parent(a);
      },
      onVarAccess(path, keys) {
        if (keys[0] == "player" && keys[1]) {
          switch (keys[1]) {
            case "skillPointsExtra":
              return this.skillPointsExtra[keys[2]];
            case "skillPointsBought":
              return this.skillPointsBought[keys[2]];
            case "skillPointCost":
              return this.getSkillPointPrice(keys[2]);
          }
        }
        return this.parent(path, keys);
      }
    });

    ig.EVENT_STEP.BUY_CP_ARENA = ig.EventStepBase.extend({
      element: null,
      amount: 0,
      _wm: new ig.Config({
        attributes: {
          element: {
            _type: "String",
            _info: "Element that gets the CP",
            _select: sc.ELEMENT
          },
          amount: {
            _type: "NumberExpression",
            _info: "Amount of CP to give.",
            _default: 1
          }
        },
        label: function() {
          return "<b>Buy CP: </b> <em>" + wmPrint("Element", this.element) + "</em> x" + this.amount
        }
      }),
      init: function(a) {
        this.element = sc.ELEMENT[a.element] || sc.ELEMENT.NEUTRAL;
        this.amount = a.amount || 1;
      },
      start: function() {
        var a = ig.Event.getExpressionValue(this.amount);
        var cost = sc.model.player.getSkillPointPrice(this.element);

        sc.arena.removeArenaCoins(cost);
        sc.model.player.skillPointsBought[this.element]++;
        sc.model.player.addSkillPoints(a, this.element, false, true)
      }
    });
    ig.EVENT_STEP.ADD_COINS = ig.EventStepBase.extend({
      amount: 0,
      _wm: new ig.Config({
        attributes: {
          amount: {
            _type: "NumberExpression",
            _info: "Amount of Coins to give.",
            _default: 1000
          }
        },
        label: function() {
          return "<b>Add Coins: </b> <em>" + wmPrint("Element", this.element) + "</em> x" + this.amount
        }
      }),
      init: function(a) {
        this.amount = a.amount || 1;
      },
      start: function() {
        var a = ig.Event.getExpressionValue(this.amount);

        sc.arena.coins += a;
      }
    });
    
  }

  onPreUpdate(){
    if(ig.input.state('DEBUG_ADD_CP')) new ig.EVENT_STEP.ADD_COINS({amount: 1000}).start();
  }
}
